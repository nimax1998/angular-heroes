import { Hero } from './hero';

export const Heroes: Hero[] = [
   { id: 1, name: 'batman' },
   { id: 2, name: 'supeman' },
   { id: 3, name: 'ironman' },
   { id: 4, name: 'test hero' },
   { id: 5, name: 'unkown' },
   { id: 6, name: 'batman again' },
   { id: 7, name: 'some random name' },
   { id: 8, name: 'unknown2' },
   { id: 9, name: 'and last' },
];
